import React from 'react';
import {shallow, configure} from 'enzyme';
import MyComponent from '../src/components/MyComponent';
import Button from '../src/components/Button';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

describe('<MyComponent/>', () => {
  it('renders three <Button/> components', () => {
    const wrapper = shallow(<MyComponent />);
    expect(wrapper.find(Button)).toHaveLength(3);
  });

  it('renders contains <Button/> components', () => {
    const wrapper = shallow(<MyComponent />);
    expect(wrapper.contains(<Button label="Button 1" />)).toEqual(true);
  });
});
