import React from 'react';
import {StatusBar, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Provider} from 'react-redux';
import AppStack from './src/navigator/AppStack';
import store from './src/store';

export default function App() {
  return (
    <Provider store={store}>
      <StatusBar barStyle="dark-content" />
      <AppStack />
    </Provider>
  );
}
