import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import MainNavigator from './MainNavigator';
import {NavigationContainer} from '@react-navigation/native';
import {connect} from 'react-redux';
import Login from '../screens/Login';
import RegisterScreen from '../screens/RegisterScreen';

const Stack = createStackNavigator();

function AppStack(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {props.sudahLogin ? (
          <Stack.Screen
            options={{headerShown: false}}
            name="Main"
            component={MainNavigator}
          />
        ) : (
          <>
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Register" component={RegisterScreen} />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const mapStateToProps = (state) => ({
  sudahLogin: state.auth.isLoggedIn,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AppStack);
