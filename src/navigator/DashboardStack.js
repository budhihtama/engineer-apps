import React from 'react';
import Dashboard from '../screens/Dashboard';
import {createStackNavigator} from '@react-navigation/stack';
import NewsItem from '../screens/NEWS';

const StackDash = createStackNavigator();

export default function DashboardStack() {
  return (
    <StackDash.Navigator>
      <StackDash.Screen name="Dashboard" component={Dashboard} />
      <StackDash.Screen name="News" component={NewsItem} />
    </StackDash.Navigator>
  );
}
