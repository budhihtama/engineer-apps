import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import DashboardStack from '../navigator/DashboardStack';
import News from '../screens/NEWS';
import TodoScreen from '../screens/todoScreen';
import Login from '../screens/Login';

const Tab = createBottomTabNavigator();
export default class MainNavigator extends Component {
  render() {
    return (
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused ? 'home-outline' : 'home';
            } else if (route.name === 'News') {
              iconName = focused ? 'newspaper-outline' : 'newspaper';
            } else if (route.name === 'TodoScreen') {
              iconName = focused ? 'person-outline' : 'person';
            }
            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'skyblue',
          inactiveTintColor: 'gray',
        }}>
        <Tab.Screen name="Home" component={DashboardStack} />
        <Tab.Screen name="News" component={News} />
        <Tab.Screen name="TodoScreen" component={TodoScreen} />
        {/* <Tab.Screen name="PROFILE" component={Profile} /> */}
      </Tab.Navigator>
    );
  }
}
