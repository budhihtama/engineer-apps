import React from 'react';
import {View, Button} from 'react-native';
import Jokes from '../components/Jokes.component';
import Weather from '../components/Weather.component';
import Slider from '../components/Slider';

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View>
        <Jokes />
        <Weather />
        <Slider />
        <Button
          onPress={() => this.props.navigation.navigate('NEWS')}
          title="Press for news info!"
          color="red"
        />
      </View>
    );
  }
}
