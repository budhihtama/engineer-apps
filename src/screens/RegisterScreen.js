import React, {useState} from 'react';
import {View, TextInput, Button} from 'react-native';
import { connect } from 'react-redux';

function RegisterScreen(props) {
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const [firstname, setFirstname] = useState(null);
  const [lastname, setLastname] = useState(null);
  
  const checkRegister = () => {
      if (!username) {
          setMessage('Username is required');
      } else if (!password) {
          setMessage('Password is required');
      } else {
          setMessage (null);

          const dataRegister = {
              username: username,
              password: password,
              first_name: firstname,
              last_name: lastname,
          };
          props.processRegister(dataRegister);
        }
  }

  return (
    <View>
      <TextInput
        placeholder="username"
        onChangeText={(text) => setUsername(text)}
        value={username}
      />

      <TextInput
        placeholder="password"
        onChangeText={(text) => setPassword(text)}
        value={password}
      />

      <TextInput
        placeholder="firstname"
        onChangeText={(text) => setFirstname(text)}
        value={firstname}
      />

      <TextInput
        placeholder="lastname"
        onChangeText={(text) => setLastname(text)}
        value={lastname}
      />

      <Button title="REGISTER" onPress={checkRegister()}/>
    </View>
  );
}

const mapStateToProps =(state) => ({

});

const mapDispatchToProps =(dispatch) => ({
    processRegister: (data) => dispatch({type: 'REGISTER', payload: data}),

});

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);