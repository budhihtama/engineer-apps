import React, {useState} from 'react';
import {StyleSheet, View, Image, Text, TextInput, Button} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {connect} from 'react-redux';

function Login(props) {
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const [message, setMessage] = useState(null);

  const login = () => {
    if (!username) {
      setMessage('Username wajib diisi');
    } else if (!password) {
      setMessage('Password wajib diisi');
    } else {
      props.processLogin(username);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image style={styles.logo} source={require('../images/90603.png')} />
        <Text style={styles.title}>
          An app made for engineer using React Native
        </Text>
      </View>
      <View style={styles.container}>
        <View style={styles.inputview}>
          <TextInput
            placeholder="username"
            onChangeText={(text) => setUsername(text)}
            style={styles.input}
          />
        </View>
        <View style={styles.inputview}>
          <TextInput
            placeholder="password"
            onChangeText={(text) => setPassword(text)}
            style={styles.input}
            secureTextEntry={true}
          />
        </View>
      </View>
      <Button onPress={() => login()} title="Login" color="red" />
      <TouchableOpacity
        onPress={() => props.navigation.navigate('Register')}
        style={{marginTop: 20, padding: 10}}>
        <Text style={{textAlign: 'center'}}>
          Dont have an account? Create new one!
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3797a4',
  },

  logo: {
    width: 100,
    height: 100,
  },

  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
  },
});

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
  processLogin: (textUsername) =>
    dispatch({type: 'LOGIN', username: textUsername}),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
