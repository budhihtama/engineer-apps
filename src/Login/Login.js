import React, {Component} from 'react';
import {StyleSheet, View, Image, Text, TextInput, Button} from 'react-native';


export default class Login extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image style={styles.logo} source={require('../images/90603.png')} />
          <Text style={styles.title}>
            An app made for engineer using React Native
          </Text>
        </View>
        {/* <Button
          onPress={() => this.props.navigation.navigate('Main')}
          title="Go to Dashboard"
          color="red"
        /> */}
        <TextInput>Login</TextInput>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3797a4',
  },

  logo: {
    width: 100,
    height: 100,
  },

  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
  },

  title: {
    color: '#fff',
    marginTop: 10,
    width: 160,
    textAlign: 'center',
  },
});
