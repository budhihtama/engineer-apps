import React from 'react';
import {StyleSheet, View, TextInput} from 'react-native';

export default class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      email: '',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.inputview}>
          <TextInput
            placeholder="email"
            onChangeText={(text) => this.setState({email: text})}
            style={styles.input}
          />
        </View>
        <View style={styles.inputview}>
          <TextInput
            placeholder="username"
            onChangeText={(text) => this.setState({username: text})}
            style={styles.input}
          />
        </View>
        <View style={styles.inputview}>
          <TextInput
            placeholder="password"
            onChangeText={(text) => this.setState({password: text})}
            style={styles.input}
            secureTextEntry={true}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },

  inputview: {
    justifyContent: 'center',
    borderRadius: 25,
    backgroundColor: 'white',
    padding: 10,
    marginBottom: 10,
    width: '80%',
  },
  input: {
    color: 'black',
  },
});
