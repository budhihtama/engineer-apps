import React from 'react';
import Button from './Button';


const MyComponent = (props) => {
  return (
    <>
      <Button label="Button 1" />
      <Button label="Button 2" />
      <Button label="Button 3" />
    </>
  );
};

export default MyComponent;
