 
 import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import axios from 'axios';

export default class Jokes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listJokes: [],
      selected_jokes: {},
    };
  }

  componentDidMount() {
    axios
      .get('http://localhost:5000/jokes')
      .then((res) => {
        const jokes_data = res.data;
        this.setState({listJokes: jokes_data});
        this.interval = setInterval(() => {
          const randomData = Math.floor(
            Math.random() * this.state.listJokes.length,
          );
          this.setState({selected_jokes: this.state.listJokes[randomData]});
        }, 3000);
      })
      .catch((err) => {
        console.log('Error');
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Text style={styles.title}>JOKES FOR YOU</Text>
        </View>

        {/* <View>
          <Text style={styles.list}>See All</Text>
        </View> */}

        <View>
          <Text style={styles.text}>{this.state.selected_jokes.jokes}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    color: 'navy',
    fontSize: 30,
    fontWeight: 'bold',
  },

  text: {
    textAlign: 'center',
    color: '#fff',
  },

  container: {
    backgroundColor: '#3797a4',
    borderColor: '#3797a4',
    borderRadius: 10,
    display: 'flex',
    flexDirection: 'column',
    padding: 10,
    margin: 10,
  }
});
