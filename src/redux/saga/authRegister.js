import {put, takeLatest} from 'redux-saga/effects';
import axios from 'axios';

function* register(action) {
  try {
    //call api ke server untuk register
    const resRegister = yield axios({
      method: 'POST',
      url: 'http://localhost:3000/auth/register',
      data: action.payload,
    });

    if (resRegister && resRegister.data) {
      // save data to store (reducer)
      yield put({type: 'REGISTER_SUCCESS'});
      // show message berhasil
    } else {
      // show message error
    }
  } catch (err) {}
}

function* authRegisterSaga() {
  yield takeLatest('REGISTER', register);
}
