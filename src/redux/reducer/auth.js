const initialState = {
  username: null,
  isLoggedIn: false,
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN': {
      return {
        isLoggedIn: true,
        username: action.username,
      };
    }
    default:
      return state;
  }
};
export default auth;
